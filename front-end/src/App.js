import Layout from './components/layout'
import React from 'react';


function App() {
  
  return (
    <div>
      <Layout/>
    </div>
  );
}

export default App;
