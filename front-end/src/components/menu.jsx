import React, {useState} from 'react';
import CarComponent from "./pages/carComponent"
import RealEstateComponent from './pages/realEstateComponent';
import NumberComponent from './pages/numberComponent';
import PhoneComponent from './pages/phoneComponent';
import NotebookComponent from './pages/notebookComponent';
import ComputerComponent from './pages/computerComponent';

const Menu = () => {
  const [contentState, setContentState] = useState("realEstate")
  let selectedComponent = null;
  const options = ["realEstate", "car", "number", "phone", "notebook", "pc"];


  if (contentState === 'realEstate') {
    selectedComponent = <RealEstateComponent />;
  } else if (contentState === 'car') {
    selectedComponent = <CarComponent />;
  }else if(contentState === 'number'){
    selectedComponent = <NumberComponent/>;
  }else if(contentState === 'phone'){
    selectedComponent = <PhoneComponent/>;
  }else if(contentState === 'notebook'){
    selectedComponent = <NotebookComponent/>;
  }else if(contentState === 'pc'){
    selectedComponent = <ComputerComponent/>;
  }

  const customizeName = (value) => {
    if(value === "realEstate"){
      return 'Үл хөдлөх';
    }else if(value === 'car'){
      return 'Автомашин'
    }else if(value === 'number'){
      return 'Утасны дугаар'
    }else if(value === 'phone'){
      return 'Гар утас'
    }else if(value === 'notebook'){
      return 'Нөүтбүүк'
    }else if(value === 'pc'){
      return 'Суурин компьютер'
    }
  }

  return (
    <div>
      <nav className="bg-blue-600 p-4">
        <div className="container mx-auto">
          <div className="flex space-x-4 justify-evenly">
            {options.map((option) => (
              <button
                key={option}
                onClick={() => setContentState(option)}
                className={`option ${contentState === option ? 'text-white cursor-pointer p-4 shadow-md bg-blue-500' : 'text-white cursor-pointer'}`}
              >
                {customizeName(option)}
              </button>
            ))}
          </div>
          
        </div>
      </nav>
      {selectedComponent}
    </div>
  );
}

export default Menu;
