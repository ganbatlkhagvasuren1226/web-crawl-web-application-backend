import requests
import urllib.request
from bs4 import BeautifulSoup
import pandas as pd
import datetime
from tqdm import tqdm


# Utga awahad hereglej bui helper function
def findFeature(li_list, header):
  ret = 'NA'
  for li in li_list:
    text = li.text.strip()
    if text.startswith(header):
      return text[len(header) + 1:]
  return ret


# bas neg helper function
def extractTprice(price):
  ans = .0
  if 'сая ₮' in price:
    ans = float(price.split('сая ₮')[0].strip().replace(',','.')) 
  elif 'Тэрбум ₮' in price:
    ans = float(price.split('Тэрбум ₮')[0].strip().replace(',','.')) * 1000
  else:
    return None
  return ans


# odoo ene hesgiig l function bolgoroi and dotor n deed 2 function hereglej baigaa
# searchValue = "120+мянгат"

def getApartmentData(value):
	app_list = []

	#Last page olj baigaa heseg

	response = requests.get(f"https://www.unegui.mn/l-hdlh/l-hdlh-zarna/oron-suuts-zarna/?type_view=line&page=1&q={value}")
	print(response)
	if response.status_code == 200:
	    soup = BeautifulSoup(response.text, 'html.parser')
	    paginationElement = soup.find('ul', class_='number-list')
	    if paginationElement is not None:
	        pageNumbers = paginationElement.find_all('a')
	        lastPage = int(pageNumbers[-1].text)
	    else:
	        lastPage=1


	#pagination-ii daguu guij url-uuda tsuglulj baigaa heseg
	        
	for i in tqdm(range(1, lastPage+1)): 
	    url = f"https://www.unegui.mn/l-hdlh/l-hdlh-zarna/oron-suuts-zarna/?type_view=line&page={i}&q={value}"
	    response = requests.get(url)
	    if response.status_code != 200:
	        print(response.status_code)
	        print('error ',url)
	        continue
	    soup = BeautifulSoup(response.text,"html.parser")
	    li_list = soup.find_all("li", {"class": "announcement-container"})
	    for li in li_list:
	        a = li.find('a')
	        appartment_url = 'https://www.unegui.mn'+a['href']
	        app_list.append(appartment_url)


	#Dawhatssan datag arilgah zorilgoor set bolgow

	app_set = set(app_list)


	#Url bureer damjij fielduud awj baigaa heseg

	it = 0
	rows= []
	for url in tqdm(app_set): 
	  it += 1
	  if it > len(app_set):
	    break
	  response = requests.get(url)
	  if response.status_code != 200:
	   print(response.status_code)
	   print('error ',url)
	   continue
	  soup = BeautifulSoup(response.text,"html.parser")
	  title = soup.find("h1", {"class": "title-announcement"}).text.strip()
	  priceRaw = soup.find("div", {"class": "announcement-price__cost"}).text.strip()
	  price1 = extractTprice(priceRaw)

	  li_class = soup.find_all("li")
	  location_div =  soup.find("div", {"class": "announcement-meta__left"})
	  location = location_div.find("span").text.strip()
	  spaceRaw = findFeature(li_class,'Талбай:')
	  space =spaceRaw.split('м')[0].strip()
	  balcony = findFeature(li_class, 'Тагт:')
	  built_year = findFeature(li_class, 'Ашиглалтанд орсон он:')
	  garage = findFeature(li_class, 'Гараж:')
	  window = findFeature(li_class, 'Цонх:')
	  building_floor = findFeature(li_class, 'Барилгын давхар:')
	  floor = findFeature(li_class, 'Хэдэн давхарт:')
	  ground_floor = findFeature(li_class, 'Шал:')
	  leasing = findFeature(li_class, 'Лизингээр авах боломж:')
	  building_process = findFeature(li_class, 'Барилгын явц:')
	  
	  if float(price1) < 15:
	    priceTotal = float(price1) * float(space)
	    priceMsq = float(price1)
	  else:
	    priceTotal = float(price1)
	    priceMsq = float(price1) / float(space)
	    
	  url = url
	  rows.append([title, location, priceTotal, priceMsq, space, building_floor, floor, garage, window, ground_floor, leasing, url])



	# current_datetime = datetime.datetime.strftime("%Y-%m-%d")
	df=pd.DataFrame(rows,columns=['Гарчиг', 'Байршил', 'Үнэ нийт/Сая ₮/', 'Үнэ мкв/Сая ₮/', 'Мкв', 'Барилгын давхар', 'Давхар', 'Гараж', 'Цонх', 'Шал', 'Лизинг', 'Линк'])
	# file_name = f"apartment_list"
	df.to_excel("apartment_list.xlsx")

# getApartmentData(searchValue)