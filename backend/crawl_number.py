import requests
import urllib.request
from bs4 import BeautifulSoup
import pandas as pd
import datetime
from tqdm import tqdm

# Utga awahad hereglej bui helper function
def findFeature(li_list, header):
  ret = 'NA'
  for li in li_list:
    text = li.text.strip()
    if text.startswith(header):
      return text[len(header) + 1:]
  return ret


# bas neg helper function
def extractTprice(price):
  ans = .0
  if 'сая ₮' in price:
    ans = float(price.split('сая ₮')[0].strip().replace(',','.')) 
  
  else:
    return None
  return ans


# dugaarin une ynzlahad zoriulsan helper function
def extractNumberPrice(price):
  ans = .0
  if 'сая ₮' in price:
    ans = float(price.split('сая ₮')[0].strip().replace(',','.'))
  
  return ans


# odoo ene hesgiig l function bolgoroi and dotor n deed 2 function hereglej baigaa


def getNumberData(value):
	app_list = []

	#Last page olj baigaa heseg

	response = requests.get(f"https://www.unegui.mn/utas--dugaar/gar-utasnyi-dugaar/?page=1&q={value}")
	print(response)
	if response.status_code == 200:
	    soup = BeautifulSoup(response.text, 'html.parser')
	    paginationElement = soup.find('ul', class_='number-list')
	    if paginationElement is not None:
	        pageNumbers = paginationElement.find_all('a')
	        lastPage = int(pageNumbers[-1].text)
	    else:
	        lastPage=1


	#pagination-ii daguu guij url-uuda tsuglulj baigaa heseg
	        
	for i in tqdm(range(1, lastPage+1)): 
	    url = f"https://www.unegui.mn/utas--dugaar/gar-utasnyi-dugaar/?page={i}&q={value}"
	    response = requests.get(url)
	    if response.status_code != 200:
	        print(response.status_code)
	        print('error ',url)
	        continue
	    soup = BeautifulSoup(response.text,"html.parser")
	    li_list = soup.find_all("li", {"class": "announcement-container"})
	    for li in li_list:
	        a = li.find('a')
	        appartment_url = 'https://www.unegui.mn'+a['href']
	        app_list.append(appartment_url)


	#Dawhatssan datag arilgah zorilgoor set bolgow
    
	app_set = set(app_list)


	#Url bureer damjij fielduud awj baigaa heseg

	it = 0
	rows= []
	for url in tqdm(app_set): 
	  it += 1
	  if it > len(app_set):
	    break
	  print(url)
	  response = requests.get(url)
	  if response.status_code != 200:
	   print(response.status_code)
	   print('error ',url)
	   continue
	  soup = BeautifulSoup(response.text,"html.parser")
	  title = soup.find("h1", {"class": "title-announcement"}).text.strip()
	  priceRaw = soup.find("div", {"class": "announcement-price__cost"}).text.strip()
	  price = extractNumberPrice(priceRaw)

	  li_class = soup.find_all("li")
	  location_div =  soup.find("div", {"class": "announcement-meta__left"})
	  location = location_div.find("span").text.strip()
	  url = url
	    
	  rows.append([title, location, price, url])



	df=pd.DataFrame(rows,columns=['Гарчиг', 'Байршил', 'Үнэ /сая ₮/', 'Линк'])
	df.to_excel("number_list.xlsx")
	