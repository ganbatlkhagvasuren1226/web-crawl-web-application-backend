from flask import Flask, request, jsonify,send_file
from flask_cors import CORS
from crawl_apartment import getApartmentData
from crawl_car import getCarData
from crawl_number import getNumberData
from crawl_pc import getPcData
from crawl_notebook import getNotebookData
from crawl_phone import  getPhoneData
import json

app = Flask(__name__)
CORS(app) 
@app.route('/')
def index():
    return "hello, world"

@app.route('/your-endpoint', methods=['POST'])
def your_endpoint():
    data = request.json  # リクエストデータを取得

    # 必要な処理を実行
    if 'phoneNumber' in data:
        phone_number = data['phoneNumber']
        getNumberData(phone_number)
        file_path = 'number_list.xlsx'
        try:
            return send_file(
            file_path,
            as_attachment=True,
            download_name='number_list.xlsx',  # ダウンロードファイル名を指定
            mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'  # ファイルのMIMEタイプを指定
        )
        except Exception as e:
            return {'error': str(e)}, 500
    else:
        print('キー "phoneNumber" が存在しません。')
    if 'realEstateComponent' in data:
        realEstateComponent = data['realEstateComponent']
        getApartmentData(realEstateComponent)
        file_path = 'apartment_list.xlsx'
        try:
            return send_file(
            file_path,
            as_attachment=True,
            download_name='apartment_list.xlsx',  # ダウンロードファイル名を指定
            mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'  # ファイルのMIMEタイプを指定
        )
        except Exception as e:
            return {'error': str(e)}, 500
    else:
        print('キー "phoneNumber" が存在しません。')
    if 'carComponent' in data:
        carComponent = data['carComponent']
        print("carComponent",carComponent)
        getCarData(carComponent)
        file_path = 'car_list.xlsx'
        try:
            return send_file(
            file_path,
            as_attachment=True,
            download_name='car_list.xlsx',  # ダウンロードファイル名を指定
            mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'  # ファイルのMIMEタイプを指定
        )
        except Exception as e:
            return {'error': str(e)}, 500
    else:
        print('キー "phoneNumber" が存在しません。')
    if 'pc' in data:
        pc = data['pc']
        getPcData(pc)
        file_path = 'pc_list.xlsx'
        try:
            return send_file(
            file_path,
            as_attachment=True,
            download_name='pc_list.xlsx',  # ダウンロードファイル名を指定
            mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'  # ファイルのMIMEタイプを指定
        )
        except Exception as e:
            return {'error': str(e)}, 500
    else:
        print('キー "pc" が存在しません。')
    if 'notebook' in data:
        notebook = data['notebook']
        getNotebookData(notebook)
        file_path = 'notebook_list.xlsx'
        try:
            return send_file(
            file_path,
            as_attachment=True,
            download_name='notebook_list.xlsx',  # ダウンロードファイル名を指定
            mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'  # ファイルのMIMEタイプを指定
        )
        except Exception as e:
            return {'error': str(e)}, 500
    else:
        print('キー "pc" が存在しません。')
    if 'phone' in data:
        phone = data['phone']
        getPhoneData(phone)
        file_path = 'phone_list.xlsx'
        try:
            return send_file(
            file_path,
            as_attachment=True,
            download_name='phone_list.xlsx',  # ダウンロードファイル名を指定
            mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'  # ファイルのMIMEタイプを指定
        )
        except Exception as e:
            return {'error': str(e)}, 500
    else:
        print('キー "pc" が存在しません。')
    return jsonify(data)  # レスポンスをJSON形式で返す

if __name__ == '__main__':
    app.run()