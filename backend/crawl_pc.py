import requests
import urllib.request
from bs4 import BeautifulSoup
import pandas as pd
import datetime
from tqdm import tqdm
import re
from datetime import datetime, timedelta  # 追加

# Utga awahad hereglej bui helper function
def findFeature(li_list, header):
    ret = 'NA'
    for li in li_list:
        text = li.text.strip()
        if text.startswith(header):
            return text[len(header) + 1:]
    return ret

def extractPcPrice(price):
    pattern1 = r'(\d{1,3}(?:,\d{3})*(?:\.\d*)? \₮)'
    pattern2 = r'(\d{1,3}(?:,\d{3})*(?:\.\d+)? сая ₮)'
    pattern3 = r'(\d{1,3}(?:,\d{3})*(?:\.\d+)? Тэрбум ₮)'
    match1 = re.search(pattern1, price)
    match2 = re.search(pattern2, price)
    match3 = re.search(pattern3, price)

    if match1:
        fixed = match1.group(0).strip()
    elif match2:
        fixed = match2.group(0).strip() 
    elif match3:
        fixed = match3.group(0).strip() 
    else:
        return None
    ans = .0
    if 'сая ₮' in fixed:
        ans = float(fixed.split('сая ₮')[0].strip().replace(',','.')) 
    elif 'Тэрбум ₮' in price:
        ans = float(fixed.split('Тэрбум ₮')[0].strip().replace(',','.')) * 1000
    else:
        ans = float(fixed.split('₮')[0].strip().replace(',','.')) / 1000
    return ans

def customizeViewsCount(count):
    match = re.search(r'\d+', count)
    
    if match:
        number = int(match.group())
        return number
    else:
        return 'NA'

def extract_date_from_string(input_string):
    input_string = input_string.replace('Өчигдөр', datetime.now().strftime('%Y-%m-%d'))
    input_string = input_string.replace('Өнөөдөр', (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d'))

    # Use regular expression to find a date in 'yyyy-mm-dd' format
    match = re.search(r'\d{4}-\d{2}-\d{2}', input_string)
    
    if match:
        # Extract the matched date string
        date_str = match.group()
        
        try:
            # Parse the date string into a datetime object
            date_obj = datetime.strptime(date_str, '%Y-%m-%d')
            return date_obj.date()
        except ValueError:
            return 'NA'
    else:
        return 'NA'

# odoo ene hesgiig l function bolgoroi and dotor n deed 2 function hereglej baigaa

def getPcData(value):
    app_list = []

    # Last page olj baigaa heseg
    response = requests.get(f"https://www.unegui.mn/kompyuter-busad/desktop/?type_view=line&page=1&q={value}")
    # print(response)
    if response.status_code == 200:
        soup = BeautifulSoup(response.text, 'html.parser')
        paginationElement = soup.find('ul', class_='number-list')

        if paginationElement is not None:
            pageNumbers = paginationElement.find_all('a')
            lastPage = int(pageNumbers[-1].text)
        else:
            lastPage=1

    for i in tqdm(range(1, lastPage+1)):
        url = f"https://www.unegui.mn/kompyuter-busad/desktop/?type_view=line&page={i}&q={value}"
        response = requests.get(url)
        if response.status_code != 200:
            print(response.status_code)
            print('error ',url)
            continue
        soup = BeautifulSoup(response.text,"html.parser")
        li_list = soup.find_all("li", {"class": "announcement-container"})
        for li in li_list:
            a = li.find('a')
            appartment_url = 'https://www.unegui.mn'+a['href']
            app_list.append(appartment_url)

    # Dawhatssan datag arilgah zorilgoor set bolgow
    app_set = set(app_list)

    # Url bureer damjij fielduud awj baigaa heseg
    it = 0
    rows= []
    for url in tqdm(app_set): 
        it += 1
        if it > len(app_set):
            break
        print(url)
        response = requests.get(url)
        if response.status_code != 200:
            print(response.status_code)
            print('error ',url)
            continue
        soup = BeautifulSoup(response.text,"html.parser")
        title = soup.find("h1", {"class": "title-announcement"}).text.strip()
        priceRaw = soup.find("div", {"class": "announcement-price__cost"}).text.strip()
        price = extractPcPrice(priceRaw)
        counter_viewsRaw = soup.find("span", {"class": "counter-views"}).text.strip()
        counter_views = customizeViewsCount(counter_viewsRaw)

        date_published_raw = soup.find("span", {"class": "date-meta"}).text.strip()
        date_published = extract_date_from_string(date_published_raw)

        li_class = soup.find_all("li")
        location_div =  soup.find("div", {"class": "announcement-meta__left"})
        location = location_div.find("span").text.strip()
        is_new = findFeature(li_class,'Шинэ / Хуучин:')
        url = url
        rows.append([title, location, price, is_new, counter_views, date_published, url])

    df=pd.DataFrame(rows,columns=['Гарчиг', 'Байршил', 'Үнэ/₮/', 'Шинэ / Хуучин', 'Үзэлтийн тоо', 'Нийтлэгдсэн огноо', 'Линк'])
    sorted_df = df.sort_values(by='Нийтлэгдсэн огноо', ascending=False)
    sorted_df.to_excel("pc_list.xlsx")

# この関数を呼び出す
# getPcData('your_value_here')
